﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class DAO_KhachHang:Provider
    {
        public DataTable Get()
        {
            string strSql = "SELECT * FROM KHACHHANG";
            SqlDataAdapter da = new SqlDataAdapter(strSql, _con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        //Lấy thông tin khách hàng để hiển thị ra màn hình
        public DataTable GetForDisplay()
        {
            string strSql = "SELECT MAKHACHHANG[Mã khách hàng], TENKHACHHANG[Tên khách hàng], " +
                "CMND[CMND], SDT[Số điện thoại] FROM KHACHHANG";
            SqlDataAdapter da = new SqlDataAdapter(strSql, _con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public bool Add(DTO_KhachHang dto)
        {
            try
            {
                _con.Open();
                string maKhachHang = TaoMaKhachHang();
                string strSql = string.Format("INSERT INTO KHACHHANG(MAKHACHHANG, TENKHACHHANG, CMND, SDT) VALUES('{0}', N'{1}', '{2}', '{3}')", maKhachHang, dto.TenKhachHang, dto.CMND1, dto.SDT1);
                SqlCommand cmd = new SqlCommand(strSql, _con);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }
            catch(Exception a)
            {
                throw a;
            }
            finally
            {
                _con.Close();
            }
            return false;
        }

        //Tạo mã khách hàng với format KH000
        private string TaoMaKhachHang()
        {
            DataTable dt = this.GetAndSortDesc();
            if (dt.Rows.Count == 0)
                return "KH000" + dt.Rows.Count;
            DataRow row = dt.Rows[0];
            string maTuyenBay = row[0].ToString().Substring(2);
            int count = int.Parse(maTuyenBay) + 1;
            int temp = count;
            string strSoKhong = "";
            int dem = 0;
            while (temp > 0)
            {
                temp /= 10;
                dem++;
            }
            for (int i = 0; i < 4 - dem; i++)
            {
                strSoKhong += "0";
            }
            return "KH" + strSoKhong + count;
        }

        //Sắp thông tin khách hàng theo mã khách hàng giảm dần
        public DataTable GetAndSortDesc()
        {
            string strSql = "SELECT * FROM KHACHHANG ORDER BY MAKHACHHANG DESC";
            SqlDataAdapter da = new SqlDataAdapter(strSql, _con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        //Cập nhật thông tin khách hàng theo mã khách hàng
        public bool Update(DTO_KhachHang dto)
        {
            try
            {
                _con.Open();
                string strSql = string.Format("UPDATE KHACHHANG SET TENKHACHHANG=N'{0}', CMND='{1}', SDT='{2}' WHERE MAKHACHHANG='{3}'", dto.TenKhachHang, dto.CMND1, dto.SDT1, dto.MaKhachHang);
                SqlCommand cmd = new SqlCommand(strSql, _con);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception a)
            {

            }
            finally
            {
                _con.Close();
            }
            return false;
        }

        //Xóa thông tin khách hàng theo mã khách hàng
        public bool Delete(string str)
        {
            try
            {
                _con.Open();
                string strSql = string.Format("DELETE FROM KHACHHANG WHERE MAKHACHHANG='{0}'", str);
                SqlCommand cmd = new SqlCommand(strSql, _con);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception a)
            {

            }
            finally
            {
                _con.Close();
            }
            return false;
        }

        //Lấy thông tin khách hàng theo số CMND
        public DataTable GetOfCMND(string CMND)
        {
            DataTable dt = new DataTable();
            string strSql = string.Format("SELECT * FROM KHACHHANG WHERE CMND='{0}'", CMND);
            SqlDataAdapter da = new SqlDataAdapter(strSql, _con);
            da.Fill(dt);
            return dt;
        }

        //Lấy thông tin khách hàng theo mã khách hàng
        public DataTable SearchOfMaKhachHang(string str)
        {
            string strSql = string.Format("SELECT MAKHACHHANG[Mã khách hàng], TENKHACHHANG[Tên khách hàng], " +
                "CMND[CMND], SDT[Số điện thoại] FROM KHACHHANG " +
                "WHERE MAKHACHHANG LIKE('%{0}%')", str);
            SqlDataAdapter da = new SqlDataAdapter(strSql, _con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
    }
}
