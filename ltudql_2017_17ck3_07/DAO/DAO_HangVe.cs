﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class DAO_HangVe:Provider
    {
        public DataTable Get()
        {
            string strSql = "SELECT * FROM HANGVE ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, _con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataTable GetForDisplay()
        {
            string strSql = "SELECT MAHANGVE[Mã hạng vé], TENHANGVE[Tên hạng vé] FROM HANGVE ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, _con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public bool Add(DTO_HangVe dto)
        {
            try
            {
                _con.Open();
                string maHangVe = TaoMaHangVe();
                string strSql = string.Format("INSERT INTO HANGVE(MAHANGVE, TENHANGVE) VALUES('{0}', N'{1}')", maHangVe, dto.TenHangVe);
                SqlCommand cmd = new SqlCommand(strSql, _con);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception a)
            {
                throw a;
            }
            finally
            {
                _con.Close();
            }
            return false;
        }
        public bool Update(DTO_HangVe dto)
        {
            try
            {
                _con.Open();
                string strSql = string.Format("UPDATE HANGVE SET TENHANGVE=N'{0}' WHERE MAHANGVE='{1}'", dto.TenHangVe, dto.MaHangVe);
                SqlCommand cmd = new SqlCommand(strSql, _con);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception a)
            {

            }
            finally
            {
                _con.Close();
            }
            return false;
        }
        public bool Delete(string str)
        {
            try
            {
                _con.Open();
                string strSql = string.Format("DELETE FROM HANGVE WHERE MAHANGVE='{0}'", str);
                SqlCommand cmd = new SqlCommand(strSql, _con);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception a)
            {

            }
            finally
            {
                _con.Close();
            }
            return false;
        }
        public DataTable GetAndSortDesc()
        {
            string strSql = "SELECT * FROM HANGVE ORDER BY MAHANGVE DESC";
            SqlDataAdapter da = new SqlDataAdapter(strSql, _con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        private string TaoMaHangVe()
        {
            DataTable dt = this.GetAndSortDesc();
            if (dt.Rows.Count == 0)
                return "HV000" + dt.Rows.Count;
            DataRow row = dt.Rows[0];
            string maTuyenBay = row[0].ToString().Substring(2);
            int count = int.Parse(maTuyenBay) + 1;
            int temp = count;
            string strSoKhong = "";
            int dem = 0;
            while (temp > 0)
            {
                temp /= 10;
                dem++;
            }
            for (int i = 0; i < 4 - dem; i++)
            {
                strSoKhong += "0";
            }
            return "HV" + strSoKhong + count;
        }
        public DataTable SearchOfMaHangVe(string str)
        {
            string strSql = string.Format("SELECT MAHANGVE[Mã hạng vé], TENHANGVE[Tên hạng vé] FROM HANGVE " +
                "WHERE MAHANGVE LIKE('%{0}%')", str);
            SqlDataAdapter da = new SqlDataAdapter(strSql, _con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
    }
}
