﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Tab;

namespace GUI
{
    public partial class frmMain : Form
    {
        private DataRow rowTTNhanVien;

        public frmMain(DataRow row)
        {
            InitializeComponent();
            rowTTNhanVien = row;
            KhoiTaoGiaoDien();
        }

        private bool CheckExistForm(Form frm)
        {
            foreach (TabPage t in tabCtrlMain.TabPages)
            {
                if (frm.Text == t.Text)
                {
                    return true;
                }
            }
            return false;
        }
        private void ActiveChildForm(Form frm)
        {
            foreach (TabPage t in tabCtrlMain.TabPages)
            {
                if (frm.Text == t.Text)
                {
                    tabCtrlMain.SelectedTab = t;
                    break;
                }
            }
        }
        private TabPage CreateTabPage(Form frm)
        {
            TabPage tabPage = new TabPage { Text = frm.Text };
            tabPage.BorderStyle = BorderStyle.None;
            tabCtrlMain.TabPages.Add(tabPage);
            tabCtrlMain.SelectedTab = tabPage;
            frm.TopLevel = false;
            frm.Parent = tabPage;
            frm.BackColor = Color.White;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.Show();
            return tabPage;
        }
        private void dangNhapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDangNhap frm = new frmDangNhap();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }

        private void KhoiTaoGiaoDien()
        {
            lbMaNhanVien.Text = "Mã nhân viên: " + rowTTNhanVien[2].ToString();
            lbUsername.Text = "Username: " + rowTTNhanVien[0].ToString();
            int type = Convert.ToInt32(rowTTNhanVien[3].ToString());
            if (type == 0)
            {
                mstrMain.Enabled = true;
                toolStripMain.Enabled = true;
            }
            if (type == 1)
            {
                mstrMain.Enabled = true;
                toolStripMain.Enabled = true;
                QuanLyThongTinToolStripMenuItem.Enabled = false;
                baoCaoToolStripMenuItem.Enabled = false;
            }
        }

        private void banVeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripBtnBanVe_Click(sender, e);
        }

        private void dangXuatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn đăng xuất không?", "Chú ý", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Hide();
                Form frm = new frmDangNhap();
                frm.ShowDialog();
                this.Close();
            }
        }

        private void toolStripBtnDangXuat_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn đăng xuất không?", "Chú ý", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Hide();
                Form frm = new frmDangNhap();
                frm.ShowDialog();
                this.Close();
            }
        }

        private void themNhanVienToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmQuanLyNhanVien frm = new frmQuanLyNhanVien();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }

        private void quanLySanBayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmQuanLySanBay frm = new frmQuanLySanBay();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }

        private void quanLyMayBayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmQuanLyMayBay frm = new frmQuanLyMayBay();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }

        private void quanLyChuyenBayToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmQuanLyChuyenBay frm = new frmQuanLyChuyenBay();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }

        private void quanLyTuyenBayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmQuanLyTuyenBay frm = new frmQuanLyTuyenBay();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }

        private void thongTInNhaSanXuatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGioiThieu frm = new frmGioiThieu();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }

//<<<<<<< HEAD
//<<<<<<< HEAD
        private void toolStripBtnBanVe_Click(object sender, EventArgs e)
        {
            frmBanVe frm = new frmBanVe(rowTTNhanVien);
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }

        private void quanLyHangVeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmQuanLyHangVe frm = new frmQuanLyHangVe();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }

        private void quanLyDonGiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmQuanLyDonGia frm = new frmQuanLyDonGia();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }
//=======
        private void baoCaoThangToolStripMenuItem_Click(object sender, EventArgs e)
            {
                frmDoanhThuThang frm = new frmDoanhThuThang();
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }
//>>>>>>> BaoCaoCB
//=======
        private void traCuuChuyenBayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTraCuuChuyenBay frm = new frmTraCuuChuyenBay();
//>>>>>>> TraCuuCB
            if (!CheckExistForm(frm))
            {
                CreateTabPage(frm);
            }
            else
            {
                ActiveChildForm(frm);
            }
        }
    }
}
