﻿namespace GUI
{
    partial class frmBanVe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            //this.quanLyBanVeCBDataSet = new GUI.QuanLyBanVeCBDataSet();
            this.dtgvVe = new System.Windows.Forms.DataGridView();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTimKiem = new System.Windows.Forms.TextBox();
            this.btnChiTietGheTrong = new System.Windows.Forms.Button();
            this.txtThoiGIanBay = new System.Windows.Forms.TextBox();
            this.cboMaChuyenBay = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtGiaTien = new System.Windows.Forms.TextBox();
            this.txtTenKhachHang = new System.Windows.Forms.TextBox();
            this.txtSoGheTrong = new System.Windows.Forms.TextBox();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.txtSanBayDen = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.gbxDSVe = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCMND = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.quanLyBanVeCBDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pLANE_TICKETDataSet = new GUI.PLANE_TICKETDataSet();
            this.txtThoiGianKhoiHanh = new System.Windows.Forms.TextBox();
            this.cHUYENBAYTableAdapter = new GUI.PLANE_TICKETDataSetTableAdapters.CHUYENBAYTableAdapter();
            this.cboHangVe = new System.Windows.Forms.ComboBox();
            this.gbxThaoTac = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMaTuyenBay = new System.Windows.Forms.TextBox();
            this.txtSanBayDi = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cHUYENBAYBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gbxTTVe = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.hANGVEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            //this.hANGVETableAdapter = new GUI.QuanLyBanVeCBDataSetTableAdapters.HANGVETableAdapter();
            this.pLANETICKETDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hANGVEBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnTimKiem = new System.Windows.Forms.Button();
            this.btnTraCuu = new System.Windows.Forms.Button();
            this.btnMuaVe = new System.Windows.Forms.Button();
            this.btnDatVe = new System.Windows.Forms.Button();
            this.btnThoat = new System.Windows.Forms.Button();
            this.dTOChuyenBayBindingSource = new System.Windows.Forms.BindingSource(this.components);
            //((System.ComponentModel.ISupportInitialize)(this.quanLyBanVeCBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvVe)).BeginInit();
            this.gbxDSVe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quanLyBanVeCBDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLANE_TICKETDataSet)).BeginInit();
            this.gbxThaoTac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cHUYENBAYBindingSource)).BeginInit();
            this.gbxTTVe.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hANGVEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLANETICKETDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hANGVEBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTOChuyenBayBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // quanLyBanVeCBDataSet
            // 
            //this.quanLyBanVeCBDataSet.DataSetName = "QuanLyBanVeCBDataSet";
            //this.quanLyBanVeCBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dtgvVe
            // 
            this.dtgvVe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvVe.Location = new System.Drawing.Point(6, 78);
            this.dtgvVe.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dtgvVe.Name = "dtgvVe";
            this.dtgvVe.RowTemplate.Height = 24;
            this.dtgvVe.Size = new System.Drawing.Size(908, 664);
            this.dtgvVe.TabIndex = 9;
            this.dtgvVe.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgvVe_CellClick);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(460, 35);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 20);
            this.label18.TabIndex = 15;
            this.label18.Text = "Tìm kiếm ";
            // 
            // txtTimKiem
            // 
            this.txtTimKiem.Location = new System.Drawing.Point(546, 29);
            this.txtTimKiem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTimKiem.Name = "txtTimKiem";
            this.txtTimKiem.Size = new System.Drawing.Size(186, 26);
            this.txtTimKiem.TabIndex = 7;
            // 
            // btnChiTietGheTrong
            // 
            this.btnChiTietGheTrong.Location = new System.Drawing.Point(321, 531);
            this.btnChiTietGheTrong.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnChiTietGheTrong.Name = "btnChiTietGheTrong";
            this.btnChiTietGheTrong.Size = new System.Drawing.Size(99, 29);
            this.btnChiTietGheTrong.TabIndex = 70;
            this.btnChiTietGheTrong.Text = "Chi tiết";
            this.btnChiTietGheTrong.UseVisualStyleBackColor = true;
            this.btnChiTietGheTrong.Click += new System.EventHandler(this.btnChiTietGheTrong_Click);
            // 
            // txtThoiGIanBay
            // 
            this.txtThoiGIanBay.Location = new System.Drawing.Point(171, 272);
            this.txtThoiGIanBay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtThoiGIanBay.Name = "txtThoiGIanBay";
            this.txtThoiGIanBay.ReadOnly = true;
            this.txtThoiGIanBay.Size = new System.Drawing.Size(248, 26);
            this.txtThoiGIanBay.TabIndex = 69;
            this.txtThoiGIanBay.TabStop = false;
            // 
            // cboMaChuyenBay
            // 
            this.cboMaChuyenBay.FormattingEnabled = true;
            this.cboMaChuyenBay.Location = new System.Drawing.Point(172, 22);
            this.cboMaChuyenBay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboMaChuyenBay.Name = "cboMaChuyenBay";
            this.cboMaChuyenBay.Size = new System.Drawing.Size(248, 28);
            this.cboMaChuyenBay.TabIndex = 0;
            this.cboMaChuyenBay.SelectedValueChanged += new System.EventHandler(this.cboMaChuyenBay_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 128);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 20);
            this.label1.TabIndex = 54;
            this.label1.Text = "Sân Bay Đi";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 78);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 20);
            this.label13.TabIndex = 54;
            this.label13.Text = "Mã tuyến bay";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 29);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 20);
            this.label6.TabIndex = 54;
            this.label6.Text = "Mã chuyến bay";
            // 
            // txtGiaTien
            // 
            this.txtGiaTien.Location = new System.Drawing.Point(170, 585);
            this.txtGiaTien.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtGiaTien.Name = "txtGiaTien";
            this.txtGiaTien.ReadOnly = true;
            this.txtGiaTien.Size = new System.Drawing.Size(200, 26);
            this.txtGiaTien.TabIndex = 68;
            this.txtGiaTien.TabStop = false;
            this.txtGiaTien.TextChanged += new System.EventHandler(this.txtGiaTien_TextChanged);
            // 
            // txtTenKhachHang
            // 
            this.txtTenKhachHang.Location = new System.Drawing.Point(170, 378);
            this.txtTenKhachHang.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTenKhachHang.Name = "txtTenKhachHang";
            this.txtTenKhachHang.Size = new System.Drawing.Size(248, 26);
            this.txtTenKhachHang.TabIndex = 2;
            // 
            // txtSoGheTrong
            // 
            this.txtSoGheTrong.Location = new System.Drawing.Point(171, 532);
            this.txtSoGheTrong.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSoGheTrong.Name = "txtSoGheTrong";
            this.txtSoGheTrong.ReadOnly = true;
            this.txtSoGheTrong.Size = new System.Drawing.Size(142, 26);
            this.txtSoGheTrong.TabIndex = 3;
            // 
            // txtSDT
            // 
            this.txtSDT.Location = new System.Drawing.Point(170, 428);
            this.txtSDT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(248, 26);
            this.txtSDT.TabIndex = 3;
            // 
            // txtSanBayDen
            // 
            this.txtSanBayDen.Location = new System.Drawing.Point(171, 168);
            this.txtSanBayDen.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSanBayDen.Name = "txtSanBayDen";
            this.txtSanBayDen.ReadOnly = true;
            this.txtSanBayDen.Size = new System.Drawing.Size(248, 26);
            this.txtSanBayDen.TabIndex = 69;
            this.txtSanBayDen.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(380, 589);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 20);
            this.label14.TabIndex = 63;
            this.label14.Text = "VNĐ";
            // 
            // gbxDSVe
            // 
            this.gbxDSVe.Controls.Add(this.dtgvVe);
            this.gbxDSVe.Controls.Add(this.label18);
            this.gbxDSVe.Controls.Add(this.txtTimKiem);
            this.gbxDSVe.Controls.Add(this.btnTimKiem);
            this.gbxDSVe.Location = new System.Drawing.Point(446, 78);
            this.gbxDSVe.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gbxDSVe.Name = "gbxDSVe";
            this.gbxDSVe.Padding = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gbxDSVe.Size = new System.Drawing.Size(974, 752);
            this.gbxDSVe.TabIndex = 85;
            this.gbxDSVe.TabStop = false;
            this.gbxDSVe.Text = "Danh sách vé";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(189, 95);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 20);
            this.label5.TabIndex = 63;
            this.label5.Text = "Đặt vé";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(51, 95);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 20);
            this.label17.TabIndex = 63;
            this.label17.Text = "Mua vé";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 591);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 20);
            this.label11.TabIndex = 63;
            this.label11.Text = "Giá tiền";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 174);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 20);
            this.label3.TabIndex = 55;
            this.label3.Text = "Sân Bay Đến";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 228);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 20);
            this.label4.TabIndex = 59;
            this.label4.Text = "Thời gian khởi hành";
            // 
            // txtCMND
            // 
            this.txtCMND.Location = new System.Drawing.Point(171, 326);
            this.txtCMND.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCMND.Name = "txtCMND";
            this.txtCMND.Size = new System.Drawing.Size(248, 26);
            this.txtCMND.TabIndex = 1;
            this.txtCMND.TextChanged += new System.EventHandler(this.txtCMND_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 488);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 20);
            this.label10.TabIndex = 64;
            this.label10.Text = "Tên hạng vé";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 538);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(101, 20);
            this.label15.TabIndex = 63;
            this.label15.Text = "Số ghế trống";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(270, 95);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(146, 20);
            this.label16.TabIndex = 63;
            this.label16.Text = "Tra cứu chuyến bay";
            // 
            // quanLyBanVeCBDataSetBindingSource
            // 
            //this.quanLyBanVeCBDataSetBindingSource.DataSource = this.quanLyBanVeCBDataSet;
            //this.quanLyBanVeCBDataSetBindingSource.Position = 0;
            // 
            // pLANE_TICKETDataSet
            // 
            this.pLANE_TICKETDataSet.DataSetName = "PLANE_TICKETDataSet";
            this.pLANE_TICKETDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txtThoiGianKhoiHanh
            // 
            this.txtThoiGianKhoiHanh.Location = new System.Drawing.Point(171, 222);
            this.txtThoiGianKhoiHanh.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtThoiGianKhoiHanh.Name = "txtThoiGianKhoiHanh";
            this.txtThoiGianKhoiHanh.ReadOnly = true;
            this.txtThoiGianKhoiHanh.Size = new System.Drawing.Size(248, 26);
            this.txtThoiGianKhoiHanh.TabIndex = 69;
            this.txtThoiGianKhoiHanh.TabStop = false;
            // 
            // cHUYENBAYTableAdapter
            // 
            this.cHUYENBAYTableAdapter.ClearBeforeFill = true;
            // 
            // cboHangVe
            // 
            this.cboHangVe.FormattingEnabled = true;
            this.cboHangVe.Location = new System.Drawing.Point(171, 478);
            this.cboHangVe.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboHangVe.Name = "cboHangVe";
            this.cboHangVe.Size = new System.Drawing.Size(248, 28);
            this.cboHangVe.TabIndex = 4;
            this.cboHangVe.SelectedValueChanged += new System.EventHandler(this.cboHangVe_SelectedValueChanged);
            // 
            // gbxThaoTac
            // 
            this.gbxThaoTac.Controls.Add(this.btnTraCuu);
            this.gbxThaoTac.Controls.Add(this.btnMuaVe);
            this.gbxThaoTac.Controls.Add(this.btnDatVe);
            this.gbxThaoTac.Controls.Add(this.label16);
            this.gbxThaoTac.Controls.Add(this.label5);
            this.gbxThaoTac.Controls.Add(this.label17);
            this.gbxThaoTac.Location = new System.Drawing.Point(10, 705);
            this.gbxThaoTac.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gbxThaoTac.Name = "gbxThaoTac";
            this.gbxThaoTac.Padding = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gbxThaoTac.Size = new System.Drawing.Size(428, 126);
            this.gbxThaoTac.TabIndex = 84;
            this.gbxThaoTac.TabStop = false;
            this.gbxThaoTac.Text = "Thao tác";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 434);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 20);
            this.label9.TabIndex = 63;
            this.label9.Text = "Điện Thoại";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 278);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 20);
            this.label12.TabIndex = 59;
            this.label12.Text = "Thời gian bay";
            // 
            // txtMaTuyenBay
            // 
            this.txtMaTuyenBay.Location = new System.Drawing.Point(171, 71);
            this.txtMaTuyenBay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMaTuyenBay.Name = "txtMaTuyenBay";
            this.txtMaTuyenBay.ReadOnly = true;
            this.txtMaTuyenBay.Size = new System.Drawing.Size(248, 26);
            this.txtMaTuyenBay.TabIndex = 69;
            this.txtMaTuyenBay.TabStop = false;
            // 
            // txtSanBayDi
            // 
            this.txtSanBayDi.Location = new System.Drawing.Point(171, 122);
            this.txtSanBayDi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSanBayDi.Name = "txtSanBayDi";
            this.txtSanBayDi.ReadOnly = true;
            this.txtSanBayDi.Size = new System.Drawing.Size(248, 26);
            this.txtSanBayDi.TabIndex = 69;
            this.txtSanBayDi.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 385);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 20);
            this.label2.TabIndex = 62;
            this.label2.Text = "Tên Khách hàng";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 332);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 20);
            this.label7.TabIndex = 61;
            this.label7.Text = "CMND";
            // 
            // cHUYENBAYBindingSource
            // 
            this.cHUYENBAYBindingSource.DataMember = "CHUYENBAY";
            this.cHUYENBAYBindingSource.DataSource = this.pLANE_TICKETDataSet;
            // 
            // gbxTTVe
            // 
            this.gbxTTVe.Controls.Add(this.btnChiTietGheTrong);
            this.gbxTTVe.Controls.Add(this.txtThoiGIanBay);
            this.gbxTTVe.Controls.Add(this.cboMaChuyenBay);
            this.gbxTTVe.Controls.Add(this.label1);
            this.gbxTTVe.Controls.Add(this.label13);
            this.gbxTTVe.Controls.Add(this.label6);
            this.gbxTTVe.Controls.Add(this.txtGiaTien);
            this.gbxTTVe.Controls.Add(this.txtTenKhachHang);
            this.gbxTTVe.Controls.Add(this.txtSoGheTrong);
            this.gbxTTVe.Controls.Add(this.txtSDT);
            this.gbxTTVe.Controls.Add(this.txtSanBayDen);
            this.gbxTTVe.Controls.Add(this.label14);
            this.gbxTTVe.Controls.Add(this.label11);
            this.gbxTTVe.Controls.Add(this.label3);
            this.gbxTTVe.Controls.Add(this.label4);
            this.gbxTTVe.Controls.Add(this.txtCMND);
            this.gbxTTVe.Controls.Add(this.label10);
            this.gbxTTVe.Controls.Add(this.txtThoiGianKhoiHanh);
            this.gbxTTVe.Controls.Add(this.label15);
            this.gbxTTVe.Controls.Add(this.cboHangVe);
            this.gbxTTVe.Controls.Add(this.label9);
            this.gbxTTVe.Controls.Add(this.label12);
            this.gbxTTVe.Controls.Add(this.txtMaTuyenBay);
            this.gbxTTVe.Controls.Add(this.txtSanBayDi);
            this.gbxTTVe.Controls.Add(this.label2);
            this.gbxTTVe.Controls.Add(this.label7);
            this.gbxTTVe.Location = new System.Drawing.Point(10, 78);
            this.gbxTTVe.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gbxTTVe.Name = "gbxTTVe";
            this.gbxTTVe.Padding = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gbxTTVe.Size = new System.Drawing.Size(428, 618);
            this.gbxTTVe.TabIndex = 83;
            this.gbxTTVe.TabStop = false;
            this.gbxTTVe.Text = "Thông tin vé";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(472, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(264, 32);
            this.label8.TabIndex = 39;
            this.label8.Text = "BÁN VÉ MÁY BAY";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.btnThoat);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1372, 72);
            this.panel1.TabIndex = 82;
            // 
            // hANGVEBindingSource
            // 
            this.hANGVEBindingSource.DataMember = "HANGVE";
            this.hANGVEBindingSource.DataSource = this.quanLyBanVeCBDataSetBindingSource;
            // 
            // hANGVETableAdapter
            // 
            //this.hANGVETableAdapter.ClearBeforeFill = true;
            // 
            // pLANETICKETDataSetBindingSource
            // 
            this.pLANETICKETDataSetBindingSource.DataSource = this.pLANE_TICKETDataSet;
            this.pLANETICKETDataSetBindingSource.Position = 0;
            // 
            // hANGVEBindingSource1
            // 
            this.hANGVEBindingSource1.DataMember = "HANGVE";
            this.hANGVEBindingSource1.DataSource = this.quanLyBanVeCBDataSetBindingSource;
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.BackgroundImage = global::GUI.Properties.Resources.btnTimKiem;
            this.btnTimKiem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTimKiem.FlatAppearance.BorderSize = 0;
            this.btnTimKiem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTimKiem.Location = new System.Drawing.Point(741, 25);
            this.btnTimKiem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(38, 38);
            this.btnTimKiem.TabIndex = 8;
            this.btnTimKiem.UseVisualStyleBackColor = true;
            // 
            // btnTraCuu
            // 
            this.btnTraCuu.BackgroundImage = global::GUI.Properties.Resources.btnTimKiem;
            this.btnTraCuu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTraCuu.FlatAppearance.BorderSize = 0;
            this.btnTraCuu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTraCuu.Location = new System.Drawing.Point(315, 26);
            this.btnTraCuu.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnTraCuu.Name = "btnTraCuu";
            this.btnTraCuu.Size = new System.Drawing.Size(62, 62);
            this.btnTraCuu.TabIndex = 64;
            this.btnTraCuu.UseVisualStyleBackColor = true;
            this.btnTraCuu.Click += new System.EventHandler(this.btnTraCuu_Click);
            // 
            // btnMuaVe
            // 
            this.btnMuaVe.BackgroundImage = global::GUI.Properties.Resources.btnMua;
            this.btnMuaVe.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMuaVe.FlatAppearance.BorderSize = 0;
            this.btnMuaVe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMuaVe.Location = new System.Drawing.Point(48, 28);
            this.btnMuaVe.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnMuaVe.Name = "btnMuaVe";
            this.btnMuaVe.Size = new System.Drawing.Size(62, 62);
            this.btnMuaVe.TabIndex = 5;
            this.btnMuaVe.UseVisualStyleBackColor = true;
            this.btnMuaVe.Click += new System.EventHandler(this.btnMuaVe_Click);
            // 
            // btnDatVe
            // 
            this.btnDatVe.BackgroundImage = global::GUI.Properties.Resources.btnDat;
            this.btnDatVe.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDatVe.FlatAppearance.BorderSize = 0;
            this.btnDatVe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDatVe.Location = new System.Drawing.Point(186, 28);
            this.btnDatVe.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDatVe.Name = "btnDatVe";
            this.btnDatVe.Size = new System.Drawing.Size(62, 62);
            this.btnDatVe.TabIndex = 6;
            this.btnDatVe.UseVisualStyleBackColor = true;
            this.btnDatVe.Click += new System.EventHandler(this.btnDatVe_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnThoat.BackgroundImage = global::GUI.Properties.Resources.btnCancel;
            this.btnThoat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnThoat.FlatAppearance.BorderSize = 0;
            this.btnThoat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThoat.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat.ForeColor = System.Drawing.Color.White;
            this.btnThoat.Location = new System.Drawing.Point(1186, 5);
            this.btnThoat.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(62, 63);
            this.btnThoat.TabIndex = 40;
            this.btnThoat.UseVisualStyleBackColor = false;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // dTOChuyenBayBindingSource
            // 
            this.dTOChuyenBayBindingSource.DataSource = typeof(DTO.DTO_ChuyenBay);
            // 
            // frmBanVe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1372, 851);
            this.Controls.Add(this.gbxDSVe);
            this.Controls.Add(this.gbxThaoTac);
            this.Controls.Add(this.gbxTTVe);
            this.Controls.Add(this.panel1);
            this.Name = "frmBanVe";
            this.Text = "Bán Vé";
            this.Shown += new System.EventHandler(this.frmBanVe_Shown);
            //((System.ComponentModel.ISupportInitialize)(this.quanLyBanVeCBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvVe)).EndInit();
            this.gbxDSVe.ResumeLayout(false);
            this.gbxDSVe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quanLyBanVeCBDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLANE_TICKETDataSet)).EndInit();
            this.gbxThaoTac.ResumeLayout(false);
            this.gbxThaoTac.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cHUYENBAYBindingSource)).EndInit();
            this.gbxTTVe.ResumeLayout(false);
            this.gbxTTVe.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.hANGVEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pLANETICKETDataSetBindingSource)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.hANGVEBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTOChuyenBayBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        //private QuanLyBanVeCBDataSet quanLyBanVeCBDataSet;
        private System.Windows.Forms.DataGridView dtgvVe;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtTimKiem;
        private System.Windows.Forms.Button btnChiTietGheTrong;
        private System.Windows.Forms.TextBox txtThoiGIanBay;
        private System.Windows.Forms.ComboBox cboMaChuyenBay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtGiaTien;
        private System.Windows.Forms.TextBox txtTenKhachHang;
        private System.Windows.Forms.TextBox txtSoGheTrong;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.TextBox txtSanBayDen;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox gbxDSVe;
        private System.Windows.Forms.Button btnTimKiem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnDatVe;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCMND;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.BindingSource quanLyBanVeCBDataSetBindingSource;
        private System.Windows.Forms.BindingSource dTOChuyenBayBindingSource;
        private PLANE_TICKETDataSet pLANE_TICKETDataSet;
        private System.Windows.Forms.TextBox txtThoiGianKhoiHanh;
        private PLANE_TICKETDataSetTableAdapters.CHUYENBAYTableAdapter cHUYENBAYTableAdapter;
        private System.Windows.Forms.ComboBox cboHangVe;
        private System.Windows.Forms.Button btnTraCuu;
        private System.Windows.Forms.GroupBox gbxThaoTac;
        private System.Windows.Forms.Button btnMuaVe;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMaTuyenBay;
        private System.Windows.Forms.TextBox txtSanBayDi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.BindingSource cHUYENBAYBindingSource;
        private System.Windows.Forms.GroupBox gbxTTVe;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.BindingSource hANGVEBindingSource;
        //private QuanLyBanVeCBDataSetTableAdapters.HANGVETableAdapter hANGVETableAdapter;
        private System.Windows.Forms.BindingSource pLANETICKETDataSetBindingSource;
        private System.Windows.Forms.BindingSource hANGVEBindingSource1;
    }
}