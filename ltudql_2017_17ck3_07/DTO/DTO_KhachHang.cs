﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_KhachHang
    {
        private string maKhachHang;
        private string tenKhachHang;
        private string CMND;
        private string SDT;

        public DTO_KhachHang()
        {
        }

        public DTO_KhachHang(string maKhachHang, string tenKhachHang, string cMND, string sDT)
        {
            this.maKhachHang = maKhachHang;
            this.tenKhachHang = tenKhachHang;
            CMND = cMND;
            SDT = sDT;
        }

        public string MaKhachHang
        {
            get
            {
                return maKhachHang;
            }

            set
            {
                maKhachHang = value;
            }
        }

        public string TenKhachHang
        {
            get
            {
                return tenKhachHang;
            }

            set
            {
                tenKhachHang = value;
            }
        }

        public string CMND1
        {
            get
            {
                return CMND;
            }

            set
            {
                CMND = value;
            }
        }

        public string SDT1
        {
            get
            {
                return SDT;
            }

            set
            {
                SDT = value;
            }
        }
    }
}
