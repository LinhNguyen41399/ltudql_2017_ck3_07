﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_KhachHang
    {
        DAO_KhachHang dao = new DAO_KhachHang();

        public DataTable Get()
        {
            return dao.Get();
        }
        public DataTable GetForDisplay()
        {
            return dao.GetForDisplay();
        }
        public bool Add(DTO_KhachHang dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_KhachHang dto)
        {
            return dao.Update(dto);
        }
        public bool Delete(string str)
        {
            return dao.Delete(str);
        }
        public DataTable GetOfCMND(string maKhachHang)
        {
            return dao.GetOfCMND(maKhachHang);
        }
        public DataTable SearchOfMaKhachHang(string str)
        {
            return dao.SearchOfMaKhachHang(str);
        }
    }
}
