﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_NhanVien
    {
        DAO_NhanVien dao = new DAO_NhanVien();

        public DataTable Get()
        {
            return dao.Get();
        }
        public DataTable GetForDisplay()
        {
            return dao.GetForDisplay();
        }
        public bool Add(DTO_NhanVien dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_NhanVien dto)
        {
            return dao.Update(dto);
        }

        public bool Delete(string str)
        {
            return dao.Delete(str);
        }
        public DataTable SearchOfMaNhanVien(string str)
        {
            return dao.SearchOfMaNhanVien(str);
        }
        public DataTable GetOfUsernameAndPassword(string username, string password)
        {
            return dao.GetOfUsernameAndPassword(username, password);
        }
    }
}
