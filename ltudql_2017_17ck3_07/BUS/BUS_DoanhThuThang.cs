﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_DoanhThuThang
    {
        DAO_DoanhThuThang dao = new DAO_DoanhThuThang();

        public DataTable Get()
        {
            return dao.Get();
        }
        public bool Add(DTO_DoanhThuThang dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_DoanhThuThang dto)
        {
            return dao.Update(dto);
        }

        public bool Delete(DTO_DoanhThuThang dto)
        {
            return dao.Delete(dto);
        }
        public DataTable GetOfNam(string strNam)
        {
            return dao.GetOfNam(strNam);
        }
    }
}
