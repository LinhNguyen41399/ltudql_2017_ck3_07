﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_ChuyenBay
    {
        DAO_ChuyenBay dao = new DAO_ChuyenBay();

        public DataTable Get()
        {
            return dao.Get();
        }
        public bool Add(DTO_ChuyenBay dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_ChuyenBay dto)
        {
            return dao.Update(dto);
        }
        public bool Delete(string str)
        {
            return dao.Delete(str);
        }
        public DataTable GetOfMaChuyenBay(string maChuyenBay)
        {
            return dao.GetOfMaChuyenBay(maChuyenBay);
        }
        public DateTime GetDateTimeOfMaChuyenBay(string str)
        {
            return dao.GetDateTimeOfMaChuyenBay(str);
        }
        public DataTable GetToDisplay()
        {
            return dao.GetToDisplay();
        }
        public DataTable SearchOfMaChuyenBay(string str)
        {
            return dao.SearchOfMaChuyenBay(str);
        }
        public DataTable Search(string maSanBayDi, string maSanBayDen, DateTime ngayKHTu, DateTime ngayKHDen)
        {
            return dao.Search(maSanBayDi, maSanBayDen, ngayKHTu, ngayKHDen);
        }
    }
}
