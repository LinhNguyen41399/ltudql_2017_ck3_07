﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_TinhTrangVe
    {
        DAO_TinhTrangVe dao = new DAO_TinhTrangVe();

        public DataTable Get()
        {
            return dao.Get();
        }
        public DataTable GetForDisplayOfMaChuyenBay(string str)
        {
            return dao.GetForDisplayOfMaChuyenBay(str);
        }
        public bool Add(DTO_TinhTrangVe dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_TinhTrangVe dto)
        {
            return dao.Update(dto);
        }
        public bool Delete(DTO_TinhTrangVe dto)
        {
            return dao.Delete(dto);
        }
        public DataTable GetOfMaChuyenBay(string maChuyenBay)
        {
            return dao.GetOfMaChuyenBay(maChuyenBay);
        }
        public string GetSoGheTrongOfMaChuyenBayAndMaHangVe(string maChuyenBay, string maHangVe)
        {
            return dao.GetSoGheTrongOfMaChuyenBayAndMaHangVe(maChuyenBay, maHangVe);
        }
    }
}
