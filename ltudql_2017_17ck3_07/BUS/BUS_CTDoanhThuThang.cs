﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_CTDoanhThuThang
    {
        DAO_CTDoanhThuThang dao = new DAO_CTDoanhThuThang();

        public DataTable Get()
        {
            return dao.Get();
        }
        public bool Add(DTO_CTDoanhThuThang dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_CTDoanhThuThang dto)
        {
            return dao.Update(dto);
        }

        public bool Delete(DTO_CTDoanhThuThang dto)
        {
            return dao.Delete(dto);
        }
        public DataTable GetOfThangNam(string strThang, string strNam)
        {
            return dao.GetOfThangNam(strThang, strNam);
        }
    }
}
