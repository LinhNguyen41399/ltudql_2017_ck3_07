﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_VeChuyenBay
    {
        DAO_VeChuyenBay dao = new DAO_VeChuyenBay();

        public DataTable Get()
        {
            return dao.Get();
        }
        public DataTable GetForDisplay()
        {
            return dao.GetForDisplay();
        }
        public bool Add(DTO_VeChuyenBay dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_VeChuyenBay dto)
        {
            return dao.Update(dto);
        }
        public bool Delete(string str)
        {
            return dao.Delete(str);
        }
    }
}
