﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class QuanLyChuyenBayBUS
    {
        public DataTable LayTatCaSanPham()
        {
            try
            {
                QuanLyChuyenBayDAO dao = new QuanLyChuyenBayDAO();
                return dao.LayTatCaChuyenBay();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}
