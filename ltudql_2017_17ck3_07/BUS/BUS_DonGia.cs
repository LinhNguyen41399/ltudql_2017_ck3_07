﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_DonGia
    {
        DAO_DonGia dao = new DAO_DonGia();

        public DataTable Get()
        {
            return dao.Get();
        }
        public DataTable GetForDisplay()
        {
            return dao.GetForDisplay();
        }
        public bool Add(DTO_DonGia dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_DonGia dto)
        {
            return dao.Update(dto);
        }
        public bool Delete(DTO_DonGia dto)
        {
            return dao.Delete(dto);
        }
        public DataTable SearchOfMaTuyenBayAndMaHangVe(string maTuyenBay, string maHangVe)
        {
            return dao.SearchOfMaTuyenBayAndMaHangVe(maTuyenBay, maHangVe);
        }
    }
}
