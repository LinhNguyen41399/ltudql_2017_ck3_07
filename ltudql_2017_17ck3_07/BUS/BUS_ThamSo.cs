﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_ThamSo
    {
        DAO_ThamSo dao = new DAO_ThamSo();
        public DataTable Get()
        {
            return dao.Get();
        }
        public bool Add(DTO_ThamSo dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_ThamSo dto)
        {
            return dao.Update(dto);
        }
        public DataTable GetForDisplay()
        {
            return dao.GetForDisplay();
        }

    }
}
