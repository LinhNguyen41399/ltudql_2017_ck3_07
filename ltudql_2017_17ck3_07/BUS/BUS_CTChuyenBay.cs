﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_CTChuyenBay
    {
        DAO_CTChuyenBay dao = new DAO_CTChuyenBay();

        public DataTable Get()
        {
            return dao.Get();
        }
        public bool Add(DTO_CTChuyenBay dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_CTChuyenBay dto)
        {
            return dao.Update(dto);
        }

        public bool Delete(DTO_CTChuyenBay dto)
        {
            return dao.Delete(dto);
        }

        public DataTable TimSanBayTGCuaChuyenBay(string maChuyenBay)
        {
            return dao.TimSanBayTGCuaChuyenBay(maChuyenBay);
        }
        public DataTable GetForDisplayOfMaChuyenBay(string str)
        {
            return dao.GetForDisplayOfMaChuyenBay(str);
        }
    }
}
