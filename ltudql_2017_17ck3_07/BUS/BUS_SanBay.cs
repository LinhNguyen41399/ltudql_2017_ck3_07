﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_SanBay
    {
        DAO_SanBay dao = new DAO_SanBay();

        public DataTable Get()
        {
            return dao.Get();
        }
        public DataTable GetForDisplay()
        {
            return dao.GetForDisplay();
        }
        public bool Add(DTO_SanBay dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_SanBay dto)
        {
            return dao.Update(dto);
        }

        public bool Delete(string str)
        {
            return dao.Delete(str);
        }
        public DataTable SearchOfMaSanBay(string maSanBay)
        {
            return dao.SearchOfMaSanBay(maSanBay);
        }
        public bool CheckSanBay(string tenSanBay, string tenThanhPho)
        {
            return dao.CheckSanBay(tenSanBay, tenThanhPho);
        }
    }
}
