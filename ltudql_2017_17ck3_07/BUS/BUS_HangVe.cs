﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_HangVe
    {
        DAO_HangVe dao = new DAO_HangVe();

        public DataTable Get()
        {
            return dao.Get();
        }
        public DataTable GetForDisplay()
        {
            return dao.GetForDisplay();
        }
        public bool Add(DTO_HangVe dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_HangVe dto)
        {
            return dao.Update(dto);
        }
        public bool Delete(string str)
        {
            return dao.Delete(str);
        }
        public DataTable SearchOfMaHangVe(string str)
        {
            return dao.SearchOfMaHangVe(str);
        }
    }
}
