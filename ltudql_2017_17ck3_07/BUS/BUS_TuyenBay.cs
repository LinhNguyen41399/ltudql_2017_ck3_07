﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_TuyenBay
    {
        DAO_TuyenBay dao = new DAO_TuyenBay();

        public DataTable Get()
        {
            return dao.Get();
        }
        public DataTable GetForDisplay()
        {
            return dao.GetForDisplay();
        }
        public DataTable GetForDSTuyenBay()
        {
            return dao.GetForDSTuyenBay();
        }
        public bool Add(DTO_TuyenBay dto)
        {
            return dao.Add(dto);
        }
        public bool Update(DTO_TuyenBay dto)
        {
            return dao.Update(dto);
        }

        public bool Delete(string str)
        {
            return dao.Delete(str);
        }
        public DataTable SearchOfMaTuyenBay(string maTuyenBay)
        {
            return dao.SearchOfMaTuyenBay(maTuyenBay);
        }
        public DataTable GetOfMaTuyenBay(string str)
        {
            return dao.GetOfMaTuyenBay(str);
        }
        public DataTable GetOfMaSanBay(string maSBDi, string maSBDen)
        {
            return dao.GetOfMaSanBay(maSBDi, maSBDen);
        }
    }
}
